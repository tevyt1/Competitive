import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by tevyt on 4/24/17.
 */
public class Solution {
    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] aliceScores = Arrays.stream(reader.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
        int[] bobScores = Arrays.stream(reader.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        System.out.println(new Solver().solve(aliceScores, bobScores));
    }
}

class Solver{
    public String solve(int[] aliceScores, int[] bobScores){
        int aliceTotal = 0;
        int bobTotal = 0;
        for(int category = 0; category < 3; category++){
           if(aliceScores[category] > bobScores[category]){
              aliceTotal++;
           }else if(aliceScores[category] < bobScores[category]){
               bobTotal++;
           }
        }

        return String.format("%d %d", aliceTotal, bobTotal);
    }
}
