import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.LongStream;

/**
 * Created by tevyt on 4/24/17.
 */
public class Solution {

    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        reader.readLine();
        String[] input = reader.readLine().split(" ");
        long[] numbers = Arrays.stream(input).mapToLong(Long::parseLong).toArray();
        System.out.println(new Solver().solve(numbers));
    }
}

class Solver{
    public long solve(long[] numbers){
        return LongStream.of(numbers).sum();
    }
}
