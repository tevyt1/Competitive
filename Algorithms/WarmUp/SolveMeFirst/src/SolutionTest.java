import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by tevyt on 4/23/17.
 */
public class SolutionTest {

    @Test
    public void addSmallNumbers(){
        long result = new Solver().solve(2, 2);
        assertEquals(4, result);
    }

    @Test
    public void addLargeNumbers(){
        long a = 1000000;
        long b = 1000000;

        long result = new Solver().solve(a, b);

        assertEquals(2000000, result);
    }

    @Test
    public void addNegativeNumbers(){
        long a = 3;
        long b = -1;

        long result = new Solver().solve(a, b);

        assertEquals(result, 2);

    }

}
