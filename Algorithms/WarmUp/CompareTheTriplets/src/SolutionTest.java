import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by tevyt on 4/24/17.
 */
public class SolutionTest {

    @Test
    public void generalCase(){
        String result = new Solver().solve(new int[]{1, 4, 5}, new int[]{10, 2, 4});
        assertEquals("2 1", result );
    }

    @Test
    public void deadLock(){
        String result = new Solver().solve(new int[]{0, 0, 0}, new int[]{0, 0, 0});
        assertEquals("0 0", result);
    }

    @Test
    public void aliceSweep(){
        String result = new Solver().solve(new int[]{1, 1, 1}, new int[]{0, 0, 0});
        assertEquals("3 0", result);
    }

    @Test
    public void bobSweep(){
        String result = new Solver().solve(new int[]{0, 0, 0}, new int[]{1, 1, 1});
        assertEquals("0 3", result);
    }
}
