import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by tevyt on 4/23/17.
 */
public class Solution {

    public static void main(String[] args) throws Exception{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        long a = Long.parseLong(reader.readLine());
        long b = Long.parseLong(reader.readLine());

        long solution = new Solver().solve(a, b);
        System.out.println(solution);
    }


}

class Solver{

    public long solve(long a, long b){
        return a + b;
    }
}