import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by tevyt on 4/24/17.
 */
public class SolverTest {
    @Test
    public void generalCase(){

        long sum = new Solver().solve(new long[]{10, 20, 30});
        assertEquals(60, sum);
    }

    @Test
    public void emptyArray(){
        long sum = new Solver().solve(new long[0]);
        assertEquals(0, sum);
    }

    @Test
    public void sumToLargeAmount(){
        long sum = new Solver().solve(new long[]{10000000, 10000000, 80000000});
        assertEquals(100000000, sum);
    }
}
